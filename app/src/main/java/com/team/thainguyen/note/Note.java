package com.team.thainguyen.note;

public class Note {
    private String titile, content;

    public String getTitile() {
        return titile;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Note(String titile, String content) {
        this.titile = titile;
        this.content = content;
    }
}
