package com.team.thainguyen.note;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class SwipeControllerRecycalView extends ItemTouchHelper.SimpleCallback {

    private NoteAdapter mAdapter;

    public SwipeControllerRecycalView(int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
    }

    public SwipeControllerRecycalView(NoteAdapter adapter) {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.mAdapter = adapter;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        int position = viewHolder.getAdapterPosition();
        listenerSwipeComplateItemRecycelViewInterface.onListener(position);
    }

    public interface ListenerSwipeComplateItemRecycelView {
        void onListener(int p);
    }

    public void onListenerComplateItemRecycelView(ListenerSwipeComplateItemRecycelView interfaces) {
        listenerSwipeComplateItemRecycelViewInterface = interfaces;
    }

    public static ListenerSwipeComplateItemRecycelView listenerSwipeComplateItemRecycelViewInterface;
}