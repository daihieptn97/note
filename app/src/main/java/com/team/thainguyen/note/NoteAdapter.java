package com.team.thainguyen.note;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.MyViewHolder> {
    private ArrayList<Note> mDataset;
    private Context context;
    private int arrColor[] = {
            R.color.bgBlue,
            R.color.bgCyanNoteItem,
            R.color.bgDeepOrange,
            R.color.bgGreen,
            R.color.bgIndigo,
            R.color.bgDeepPurple,
            R.color.bgPurple,
    };

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        // create a new view
        context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_recycel_view_item, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.title.setText(mDataset.get(i).getTitile());
        myViewHolder.content.setText(mDataset.get(i).getContent());
        Random random = new Random();

        myViewHolder.lnLayout.setBackgroundColor(ContextCompat.getColor(context, arrColor[random.nextInt(arrColor.length - 1)]));
    }

    private static OnItemClickListener listener;
    private static OnItemClickLongListener longClickListener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public interface OnItemClickLongListener {
        void onItemLongClick(View itemView, int position);
    }

    public void deleteItemSwipe(int p) {
        this.notifyItemRemoved(p);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setOnItemLongClickListener(OnItemClickLongListener listener) {
        this.longClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public NoteAdapter(ArrayList<Note> myDataset) {
        this.mDataset = myDataset;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title, content;
        public LinearLayout lnLayout;

        public MyViewHolder(final View v) {
            super(v);
            title = v.findViewById(R.id.txtTitle);
            content = v.findViewById(R.id.txtContent);
            lnLayout = v.findViewById(R.id.lnItemNote);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    listener.onItemClick(v, getLayoutPosition());
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View vv) {
                    longClickListener.onItemLongClick(v, getLayoutPosition());
                    return false;
                }
            });
        }
    }
}
