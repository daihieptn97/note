package com.team.thainguyen.note;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LayoutAnimationController animation;
    private NoteAdapter mAdapter;
    private boolean type = false; // LinerLayout
    ArrayList<Note> noteArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        mapping(); // connect java to View

        loadData(noteArrayList); // load data temp

        //set adpater in recycel View
        mAdapter = new NoteAdapter(noteArrayList);
        recyclerView.setAdapter(mAdapter);

        // Set Swipe Recycel View
        SwipeControllerRecycalView swipeController = new SwipeControllerRecycalView(mAdapter);
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);


        findViewById(R.id.tgTypeListNote).setOnClickListener(new View.OnClickListener() { // changer type List
            @Override
            public void onClick(View v) {
                StaggeredGridLayoutManager gridLayoutManager;
                if (type) {
                    layoutManager = new LinearLayoutManager(getBaseContext());
                    type = false;
                    recyclerView.setLayoutManager(layoutManager);
                } else {
                    gridLayoutManager =
                            new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                    type = true;
                    recyclerView.setLayoutManager(gridLayoutManager);
                }
                recyclerView.setLayoutAnimation(animation);
            }
        });

        swipeController.onListenerComplateItemRecycelView(new SwipeControllerRecycalView.ListenerSwipeComplateItemRecycelView() {
            @Override
            public void onListener(int p) {
                mAdapter.notifyItemRemoved(p);
                noteArrayList.remove(p);

            }
        });

        mAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                Toast.makeText(MainActivity.this, "setOnItemClickListener" + noteArrayList.get(position).getTitile() + "", Toast.LENGTH_SHORT).show();
            }
        });

        mAdapter.setOnItemLongClickListener(new NoteAdapter.OnItemClickLongListener() {
            @Override
            public void onItemLongClick(View itemView, int position) {
                Toast.makeText(MainActivity.this, "Long CLick" + noteArrayList.get(position).getTitile() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadData(ArrayList<Note> noteArrayList) {

        noteArrayList.add(new Note("Todo 1", "titleclude selection in the activity lifecycle events.\n" +
                "In order to preserve selection state across the activity lifecycle events, your "));
        noteArrayList.add(new Note("Todo 9", "At the end of the event processing pipeline, the library may determine that the user is attempting to activate an item by tapping it, or is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 8", "At the end of the event processing pipeline, the library may determine that the user is attempting to activate an item by tapping it, or is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 7", "At the end of the event processing pipeline, the library may determine that the user is aor is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 6", "At the end of the event processing pipeline, the library may determy tapping it, or is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 5", "At the ene an item by tapping it, or is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 4", "At the end of the event processing pipeline, the library may determine that the user is attempting to activate an item by tapping it, or is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 3", " to activate an item by tapping it, or is attempting to drag and drop an item or set of selected items "));
        noteArrayList.add(new Note("Todo 2", "  of selected items "));
    }

    private void mapping() {
        recyclerView = findViewById(R.id.my_recycler_view);
        layoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(layoutManager);
        animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        noteArrayList = new ArrayList<>();
    }
}